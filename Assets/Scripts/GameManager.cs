﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static  GameManager Instance { get { return _instance; } }
    private static GameManager _instance;

    public GameObject Player { get { return _player; } }
    private GameObject _player;

    public static UIController UIController;

    public List<GameObject> EnemiesInGame;

    private static GameObject MainCamera;

    public int Score{ get { return _score; } }
    private int _score;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            Debug.Log("GameManager DESTROYED");
        }
        _instance = this;
        DontDestroyOnLoad(_instance);
    }

    // Start is called before the first frame update
    void Start()
    {
        _score = 0;

        _player = GameObject.FindWithTag("Player");

        UIController = GameObject.Find("Canvas").GetComponent<UIController>();

        MainCamera = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public int GetNumberOfEnemiesAlive()
    {
        return EnemiesInGame.Count;
    }

    public void IncreaseScore(int value)
    {
        _score += value;
    }

    public void ResetStage()
    {
        ResetPlayerPosition();
    }

    public void ResetPlayerPosition()
    {
        Player.GetComponent<Player>().ResetPosition();
    }

    public void GameOver()
    {
        //UIController.GameOver();
        StartCoroutine(WaitForGameOverScreen());
    }

    public void ResetGame()
    {
        GoToStartScreen();
    }

    private IEnumerator WaitForGameOverScreen()
    {
        yield return new WaitForSeconds(UIController.GameOverScreenTime);
        ResetGame();
    }

    public void GoToStartScreen()
    {
        SceneManager.LoadScene("P1_Start");
    }

    public void GoToGameScreen()
    {
        SceneManager.LoadScene("M17UF1-Prova");
    }

    public int GetScore()
    {
        return Score;
    }
}

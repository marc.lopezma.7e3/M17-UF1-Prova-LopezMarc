﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text ScoreText;

    public Text GameOverText;
    private string GameOverString;
    public int GameOverScreenTime;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        //ScoreText.text = "Score: " + GameManager.Instance.GetScore();
    }

    public void GameOver()
    {
        //StartCoroutine(DisplayGameOverScreen());
    }

    private IEnumerator DisplayGameOverScreen()
    {
        yield return new WaitForSeconds(GameOverScreenTime);
        
    }

}

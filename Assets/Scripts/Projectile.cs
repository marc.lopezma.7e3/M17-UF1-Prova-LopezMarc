﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    bool GoingUp;

    [SerializeField]
    float Speed;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.CompareTag("Player")) GoingUp = true; else GoingUp = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(GoingUp)
        {
            transform.position += new Vector3(0.00f, Speed * Time.deltaTime, 0.00f);
        }
        else
        {

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            GameManager.Instance.Player.GetComponent<Player>().Die(); 
        }
    }


}

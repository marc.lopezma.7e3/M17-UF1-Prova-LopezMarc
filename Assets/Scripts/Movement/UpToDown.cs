﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpToDown : MonoBehaviour
{

    [SerializeField]
    private int Speed;
    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0.00f, -Speed * Time.deltaTime, 0.00f);
    }

    
}

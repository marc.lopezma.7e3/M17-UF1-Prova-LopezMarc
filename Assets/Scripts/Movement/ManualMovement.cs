﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *   - Bonus: Crear un component que substitueix el 
 *   de moviment per un altre que dona el moviment 
 *   del personatge a l'usuari.
 *  */

public class ManualMovement : MonoBehaviour
{
    [SerializeField]
    float Speed;

    float horizontalInput;
    float verticalInput;

    Vector3 ToMove;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        ToMove = new Vector3(-verticalInput, horizontalInput, 0) * Speed * Time.deltaTime;
        transform.Translate(ToMove);  
    }

    public bool GetMoving()
    {
        if (MovingX() || MovingY()) return true;
        else return false;
    }

    public bool MovingX()
    {
        if (horizontalInput > 0 || horizontalInput < 0) return true;
        else return false;
    }

    public bool MovingY()
    {
        if (verticalInput > 0 || verticalInput < 0) return true;
        else return false;
    }

    private void OnEnable()
    {
        Start();
    }

    public Vector3 GetToMove()
    {
        return ToMove;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragate : Enemy
{

    // Start is called before the first frame update
    void Start()
    {
        XP = 20;
        Damage = 1;
        HitPoints = 5;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("COLLIDED");
        if (collision.CompareTag("Player"))
        {
            Debug.Log("WITH PLAYER");
            GameObject cl = collision.gameObject;
            cl.GetComponent<Player>().TakeDamage(Damage);
        }
        TakeDamage(1);
    }

    private void TakeDamage(int Damage)
    {
        HitPoints -= Damage;
        if (HitPoints <= 0)
        {
            Die();
        }
    }
}
